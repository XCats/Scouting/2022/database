#!/bin/bash

export DB_CONN_STRING='postgresql://alfs@localhost/scouting_2022'

export psql_command="psql $DB_CONN_STRING"

$psql_command -c "DROP SCHEMA public CASCADE;"
$psql_command -c "CREATE SCHEMA public;"