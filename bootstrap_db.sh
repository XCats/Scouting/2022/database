#!/bin/bash

export DB_CONN_STRING='postgresql://alfs@localhost/scouting_2022'

export psql_command="psql $DB_CONN_STRING"

$psql_command -f year_agnostic/teams.create.sql
$psql_command -f year_agnostic/events.create.sql
$psql_command -f year_agnostic/matches.create.sql