CREATE TYPE competition_level AS enum ('qm', 'ef', 'qf', 'sf', 'f');

-- subset of TBA match_simple object
CREATE TABLE match (
    id SERIAL PRIMARY KEY,
    key VARCHAR UNIQUE,
    comp_level competition_level,
    set_number INTEGER,
    match_number INTEGER,
    event_key VARCHAR,
    event_id INTEGER REFERENCES event(id)
);

CREATE TYPE ALLIANCE AS enum ('RED', 'BLUE');

CREATE TABLE match_team (
    id SERIAL PRIMARY KEY,
    match_id INTEGER REFERENCES match(id) NOT NULL ,
    team_id INTEGER REFERENCES team(id) NOT NULL,
    alliance ALLIANCE,
    UNIQUE (match_id, team_id)
);
