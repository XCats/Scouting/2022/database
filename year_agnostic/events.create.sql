-- https://github.com/the-blue-alliance/the-blue-alliance/blob/master/consts/event_type.py#L2
CREATE TYPE event_type AS ENUM ('regional', 'district', 'district_cmp', 'cmp_division', 'cmp_finals', 'district_cmp_division', 'foc', 'remote');

-- subset of TBA event_simple object
CREATE TABLE event (
    id SERIAL PRIMARY KEY,
    key VARCHAR UNIQUE, -- 2022nyro
    name VARCHAR, -- Finger Lakes Regional
    event_type EVENT_TYPE,
    event_code VARCHAR, -- nyro
    year integer, -- 2022
    start_date DATE, -- 2/9/2022
    end_date DATE -- 2/12/2022
);

CREATE TABLE team_event (
    id SERIAL PRIMARY KEY,
    event_id INTEGER REFERENCES event(id),
    team_id INTEGER REFERENCES team(id)
);