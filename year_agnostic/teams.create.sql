-- Dupe of TBA's team_simple object
CREATE TABLE team (
    id INTEGER PRIMARY KEY, -- Match team_number
    key VARCHAR, -- frc191
    team_number INTEGER UNIQUE, -- 191
    name VARCHAR, -- Xerox Corporation/Sikorsky · Lockheed Martin/ESL Federal Credit Union&Wilson Magnet High School&Wilson Foundation Academy
    nickname VARCHAR, -- X-CATS
    city VARCHAR, -- Rochester
    state_prov VARCHAR, -- New York
    country VARCHAR -- United States
);