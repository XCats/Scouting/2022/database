-- dupe of TBA Match_Score_Breakdown_2022_Alliance, with individual robot data split out 
CREATE TABLE rapid_react_match_result (
    id INTEGER PRIMARY KEY,
    match_id INTEGER,
    alliance ALLIANCE,
    auto_cargo_lower_near INTEGER,
    auto_cargo_lower_far INTEGER,
    auto_cargo_lower_blue INTEGER,
    auto_cargo_lower_red INTEGER,
    auto_cargo_upper_near INTEGER,
    auto_cargo_upper_far INTEGER,
    auto_cargo_upper_blue INTEGER,
    auto_cargo_upper_red INTEGER,
    auto_cargo_total INTEGER,
    teleop_cargo_lower_near INTEGER,
    teleop_cargo_lower_far INTEGER,
    teleop_cargo_lower_blue INTEGER,
    teleop_cargo_lower_red INTEGER,
    teleop_cargo_upper_near INTEGER,
    teleop_cargo_upper_far INTEGER,
    teleop_cargo_upper_blue INTEGER,
    teleop_cargo_upper_red INTEGER,
    teleop_cargo_total INTEGER,
    match_cargo_total INTEGER,
    auto_taxi_points INTEGER,
    auto_cargo_points INTEGER,
    auto_points INTEGER,
    quintet_achieved INTEGER,
    teleop_cargo_points INTEGER,
    endgame_points INTEGER,
    teleop_points INTEGER,
    cargo_bonus_ranking_point boolean,
    hangar_bonus_ranking_point boolean,
    foul_count INTEGER,
    tech_foul_count INTEGER,
    adjust_points INTEGER,
    foul_points INTEGER,
    rp INTEGER,
    total_points INTEGER
);

CREATE TYPE rapid_react_endgame AS enum ('traversal', 'high', 'mid', 'low', 'none');

CREATE TABLE rapid_react_robot_match_result (
    id INTEGER PRIMARY KEY,
    match_team_id INTEGER REFERENCES match_team (id),
    match_result_id INTEGER REFERENCES rapid_react_match_result (id),
    taxi boolean,
    endgame RAPID_REACT_ENDGAME
);