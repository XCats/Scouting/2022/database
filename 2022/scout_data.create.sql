-- Scouting data from the X-Cats app
CREATE TABLE rapid_react_xcats_scouted_match (
    id SERIAL PRIMARY KEY,
    match_team_id INTEGER REFERENCES match_team(id),
    username VARCHAR,
    averageTime INTEGER,
    atbs INTEGER,
    end_game_defense_rating INTEGER,
    end_game_malfunction BOOLEAN,
    end_game_malfunction_notes VARCHAR,
    end_game_performed_defense BOOLEAN,
    end_game_points INTEGER,
    end_game_result VARCHAR,
    main_game_lower_accuracy FLOAT,
    main_game_lower_made INTEGER,
    main_game_lower_miss INTEGER,
    main_game_upper_accuracy FLOAT,
    main_game_upper_made INTEGER,
    main_game_upper_miss INTEGER,
    main_game_points INTEGER
);
