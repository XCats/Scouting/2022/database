CREATE TYPE girls_of_steel_climb AS enum('Traversal (Very top)', 'High', 'Medium', 'Low', 'Nothing');

CREATE TYPE girls_of_steel_shooting_location AS enum('Tarmac', 'Launch pad', 'Anywhere else');

CREATE TYPE girls_of_steel_robot_problem AS enum('None', 'Minor', 'Major', 'Absolute');

CREATE TABLE rapid_react_girls_of_steel_scouted_match (
    id SERIAL PRIMARY KEY,
    match_team_id INTEGER REFERENCES match_team(id),
    taxi BOOLEAN,
    auto_shots_upper INTEGER,
    auto_shots_lower INTEGER,
    auto_shots_missed INTEGER,
    tele_shots_upper INTEGER,
    tele_shots_lower INTEGER,
    tele_shots_missed INTEGER,
    climb_attempt GIRLS_OF_STEEL_CLIMB,
    climb_success GIRLS_OF_STEEL_CLIMB,
    robot_problem GIRLS_OF_STEEL_ROBOT_PROBLEM,
    problem_description VARCHAR,
    additional_comments VARCHAR
);

CREATE TABLE rapid_react_girls_of_steel_shooting_locations (
    id SERIAL PRIMARY KEY,
    rapid_react_girls_of_steel_scouted_match_id INTEGER REFERENCES rapid_react_girls_of_steel_scouted_match (id),
    shooting_location GIRLS_OF_STEEL_SHOOTING_LOCATION
);